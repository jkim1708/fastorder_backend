package com.fastOrder.fastOrderProject.ServiceTests;



import com.fastOrder.fastOrderProject.models.WaiterRequestModel;
import com.fastOrder.fastOrderProject.services.WaiterRequestService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class WaiterRequestServiceTest {

    @Autowired
    private WaiterRequestService waiterRequestService;

    @Test
    void readingAllWaiterRequests(){
        ArrayList<WaiterRequestModel> waiterRequestModels = waiterRequestService.readAllWaiterRequests();
        assertThat(waiterRequestModels.size()).isEqualTo((4));
    }

    @Test
    void readingAllWaiterRequestsByCustomerTableIdTest(){
        ArrayList<WaiterRequestModel> waiterRequestModels = waiterRequestService.readAllWaiterRequestsByCustomerTableId(4);
        assertThat(waiterRequestModels.size()).isEqualTo((2));
    }

    @Test
    void readingAllWaiterRequestsByRestaurantIdTest(){
        ArrayList<WaiterRequestModel> waiterRequestModels = waiterRequestService.readAllWaiterRequestsByRestaurantId(1);
        assertThat(waiterRequestModels.size()).isEqualTo((3));
    }

    @Test
    void saveAllWaiterRequests(){
        ArrayList<WaiterRequestModel> waiterRequestModels = new ArrayList<>();
        waiterRequestModels.add(new WaiterRequestModel(1,4,"100620220",false));
        waiterRequestModels.add(new WaiterRequestModel(2,4,"110620220",false));
        waiterRequestModels.add(new WaiterRequestModel(3,5,"120620220",false));
        waiterRequestService.saveWaiterRequest(waiterRequestModels);
    }

    @Test
    void changeServedStatusTest(){
        WaiterRequestModel waiterRequestModel = new WaiterRequestModel(15);
        waiterRequestService.changeServedStatus(waiterRequestModel);
    }

    @Test
    void CustomerTableServedTest(){
        waiterRequestService.customerTableServed(4);
    }

}
