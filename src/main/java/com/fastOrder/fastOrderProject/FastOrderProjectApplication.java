package com.fastOrder.fastOrderProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ComponentScan({"com.fastOrder.fastOrderProject","controllers"})
public class FastOrderProjectApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/");

		SpringApplication.run(FastOrderProjectApplication.class, args);
	}

	@PostConstruct
	public void pc() {
	System.out.println("hi");
		}

	}


