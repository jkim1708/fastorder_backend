package com.fastOrder.fastOrderProject.services;

import com.fastOrder.fastOrderProject.entities.MenuItem;
import com.fastOrder.fastOrderProject.repositories.MenuItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuItemService {

    @Autowired
    MenuItemRepository menuItemRepository;


    //show all items of restaurants complete menu
    public List<MenuItem> getAllMenuItems(long restaurantId){
        return menuItemRepository.findByRestaurantId(restaurantId);
    }

}
