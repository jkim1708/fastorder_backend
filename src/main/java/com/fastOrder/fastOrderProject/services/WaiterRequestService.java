package com.fastOrder.fastOrderProject.services;


import com.fastOrder.fastOrderProject.entities.WaiterRequest;
import com.fastOrder.fastOrderProject.models.WaiterRequestModel;

import com.fastOrder.fastOrderProject.repositories.WaiterRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WaiterRequestService {

    @Autowired
    WaiterRequestRepository waiterRequestRepository;

    public ArrayList<WaiterRequestModel> readAllWaiterRequests(){
        ArrayList<WaiterRequestModel> waiterRequestModels = new ArrayList<>();
        List<WaiterRequest> waiterRequests = waiterRequestRepository.findAll();
        for (WaiterRequest waiterRequest : waiterRequests){
            WaiterRequestModel waiterRequestModel = new WaiterRequestModel(
                    waiterRequest.getWaiterRequestId(), waiterRequest.getCustomerTableId(),
                    waiterRequest.getRequestReceivedTimeStamp(), waiterRequest.isCustomerServed());
            waiterRequestModels.add(waiterRequestModel);
        }

        return waiterRequestModels;
    }

    public ArrayList<WaiterRequestModel> readAllWaiterRequestsByRestaurantId(long restaurantId){
        ArrayList<WaiterRequestModel> waiterRequestModels = new ArrayList<>();
        List<WaiterRequest> waiterRequests = waiterRequestRepository.findAllWaiterRequestsByRestaurantId(restaurantId);
        for (WaiterRequest waiterRequest : waiterRequests){
            WaiterRequestModel waiterRequestModel = new WaiterRequestModel(
                    waiterRequest.getWaiterRequestId(), waiterRequest.getCustomerTableId(),
                    waiterRequest.getRequestReceivedTimeStamp(), waiterRequest.isCustomerServed());
            waiterRequestModels.add(waiterRequestModel);
        }

        return waiterRequestModels;
    }

    public ArrayList<WaiterRequestModel> readAllWaiterRequestsByCustomerTableId(long customerTableId){
        ArrayList<WaiterRequestModel> waiterRequestModels = new ArrayList<>();
        List<WaiterRequest> waiterRequests = waiterRequestRepository.findAllByCustomerTableId(customerTableId);
        for (WaiterRequest waiterRequest : waiterRequests){
            WaiterRequestModel waiterRequestModel = new WaiterRequestModel(
                    waiterRequest.getWaiterRequestId(), waiterRequest.getCustomerTableId(),
                    waiterRequest.getRequestReceivedTimeStamp(), waiterRequest.isCustomerServed());
            waiterRequestModels.add(waiterRequestModel);
        }

        return waiterRequestModels;
    }


    public boolean saveWaiterRequest (ArrayList<WaiterRequestModel> waiterRequestModels){
        for (WaiterRequestModel waiterRequestModel : waiterRequestModels){
            WaiterRequest waiterRequest = new WaiterRequest(waiterRequestModel.getCustomerTableId(), waiterRequestModel.getRestaurantId(),
                    waiterRequestModel.getRequestReceivedTimeStamp(), waiterRequestModel.isCustomerServed());
            waiterRequestRepository.save(waiterRequest);
        }
        return  true;
    }

    public boolean changeServedStatus (WaiterRequestModel waiterRequestModel){
        WaiterRequest waiterRequest = waiterRequestRepository.findAllByWaiterRequestId(waiterRequestModel.getWaiterRequestId());
        waiterRequest.setCustomerServed(true);
        waiterRequestRepository.save(waiterRequest);
        return true;
    }

    public boolean customerTableServed (long customerTableId){
        ArrayList<WaiterRequestModel> waiterRequestModels = readAllWaiterRequestsByCustomerTableId(customerTableId);
        for (WaiterRequestModel waiterRequestModel :waiterRequestModels) {
            changeServedStatus(waiterRequestModel);
        }
        return true;
    }


}
