package com.fastOrder.fastOrderProject.models;


import com.fastOrder.fastOrderProject.entities.MenuItem;
import com.fastOrder.fastOrderProject.entities.WaiterRequest;

import java.util.List;


public class RestaurantModel {


    private long id;

    private String restaurantName;


    private String restaurantBusinessName;

    private String restaurantOwnerName;

    private String restaurantOwnerNumber;

    private String restaurantOwnerEmail;

    private String restaurantLocationStreet;

    private String restaurantLocationPostnumber;

    private String restaurantLocationCity;

    private String restaurantLocationCountry;


    List<MenuItem> menuItems;

    List<WaiterRequest> waiterRequests;


    public RestaurantModel(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantBusinessName() {
        return restaurantBusinessName;
    }

    public void setRestaurantBusinessName(String restaurantBusinessName) {
        this.restaurantBusinessName = restaurantBusinessName;
    }

    public String getRestaurantOwnerName() {
        return restaurantOwnerName;
    }

    public void setRestaurantOwnerName(String restaurantOwnerName) {
        this.restaurantOwnerName = restaurantOwnerName;
    }

    public String getRestaurantOwnerNumber() {
        return restaurantOwnerNumber;
    }

    public void setRestaurantOwnerNumber(String restaurantOwnerNumber) {
        this.restaurantOwnerNumber = restaurantOwnerNumber;
    }

    public String getRestaurantOwnerEmail() {
        return restaurantOwnerEmail;
    }

    public void setRestaurantOwnerEmail(String restaurantOwnerEmail) {
        this.restaurantOwnerEmail = restaurantOwnerEmail;
    }

    public String getRestaurantLocationStreet() {
        return restaurantLocationStreet;
    }

    public void setRestaurantLocationStreet(String restaurantLocationStreet) {
        this.restaurantLocationStreet = restaurantLocationStreet;
    }

    public String getRestaurantLocationPostnumber() {
        return restaurantLocationPostnumber;
    }

    public void setRestaurantLocationPostnumber(String restaurantLocationPostnumber) {
        this.restaurantLocationPostnumber = restaurantLocationPostnumber;
    }

    public String getRestaurantLocationCity() {
        return restaurantLocationCity;
    }

    public void setRestaurantLocationCity(String restaurantLocationCity) {
        this.restaurantLocationCity = restaurantLocationCity;
    }

    public String getRestaurantLocationCountry() {
        return restaurantLocationCountry;
    }

    public void setRestaurantLocationCountry(String restaurantLocationCountry) {
        this.restaurantLocationCountry = restaurantLocationCountry;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public List<WaiterRequest> getWaiterRequests() {
        return waiterRequests;
    }

    public void setWaiterRequests(List<WaiterRequest> waiterRequests) {
        this.waiterRequests = waiterRequests;
    }
}

