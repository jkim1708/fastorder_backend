package com.fastOrder.fastOrderProject.models;

public class RestaurantIdModel {
    private long restaurantId;

    public RestaurantIdModel() {
    }

    public RestaurantIdModel(long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
