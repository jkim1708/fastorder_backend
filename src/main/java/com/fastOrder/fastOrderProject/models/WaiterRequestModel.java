package com.fastOrder.fastOrderProject.models;



public class WaiterRequestModel {


    private long waiterRequestId;

    private long customerTableId;
    private long restaurantId;

    private String requestReceivedTimeStamp;

    private boolean customerServed;

    public WaiterRequestModel() {
    }

    public WaiterRequestModel(long waiterRequestId) {
        this.waiterRequestId = waiterRequestId;
    }

    public WaiterRequestModel(long customerTableId, long restaurantId) {
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
    }



    public WaiterRequestModel(long waiterRequestId, long customerTableId, String requestReceivedTimeStamp, boolean customerServed) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
        this.customerServed = customerServed;
    }

    public WaiterRequestModel(long waiterRequestId, long customerTableId, long restaurantId, String requestReceivedTimeStamp, boolean customerServed) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
        this.customerServed = customerServed;
    }

    public long getWaiterRequestId() {
        return waiterRequestId;
    }

    public void setWaiterRequestId(long waiterRequestId) {
        this.waiterRequestId = waiterRequestId;
    }

    public String getRequestReceivedTimeStamp() {
        return requestReceivedTimeStamp;
    }

    public void setRequestReceivedTimeStamp(String requestReceivedTimeStamp) {
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
    }

    public boolean isCustomerServed() {
        return customerServed;
    }

    public void setCustomerServed(boolean customerServed) {
        this.customerServed = customerServed;
    }

    public long getCustomerTableId() {
        return customerTableId;
    }

    public void setCustomerTableId(long customerTableId) {
        this.customerTableId = customerTableId;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
