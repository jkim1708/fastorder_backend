package com.fastOrder.fastOrderProject.models;

import com.fastOrder.fastOrderProject.entities.WaiterRequest;

import java.util.List;


public class CustomerTableModel {


    private long customerTableId;
    private long restaurantId;

    public CustomerTableModel() {
    }
    public CustomerTableModel(long customerTableId) {
        this.customerTableId = customerTableId;
    }
    public CustomerTableModel(long customerTableId, long restaurantId) {
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;

    }

    public long getCustomerTableId() {
        return customerTableId;
    }

    public void setCustomerTableId(long id) {
        this.customerTableId = id;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
