package com.fastOrder.fastOrderProject.controllers;

import com.fastOrder.fastOrderProject.models.CustomerTableModel;
import com.fastOrder.fastOrderProject.models.RestaurantIdModel;
import com.fastOrder.fastOrderProject.models.WaiterRequestModel;
import com.fastOrder.fastOrderProject.services.WaiterRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
public class WaiterRequestController {


    @Autowired
    WaiterRequestService waiterRequestService;

    @PostMapping("/pingWaiter")
    @CrossOrigin
    public boolean createWaiterRequest(@RequestBody ArrayList<WaiterRequestModel> waiterRequestModels) {
        System.out.println("start createRequest Controller");
        boolean result = waiterRequestService.saveWaiterRequest(waiterRequestModels);
        return result;
    }

    @PostMapping("/readAllWaiterRequests")
    @CrossOrigin
    public ArrayList<WaiterRequestModel> readAllWaiterRequests(@RequestBody RestaurantIdModel restaurantIdModel) {
        System.out.println("start readAllWaiterRequest Controller");
        ArrayList<WaiterRequestModel> waiterRequestModels = waiterRequestService.readAllWaiterRequestsByRestaurantId(restaurantIdModel.getRestaurantId());
        return waiterRequestModels;
    }

    @PostMapping("/changeWaiterRequestStatus")
    @CrossOrigin
    public boolean changeWaiterRequestStatus(@RequestBody WaiterRequestModel waiterRequestModel){
        System.out.println("start changeWaiterRequestStatus");
        waiterRequestService.changeServedStatus(waiterRequestModel);
        return true;
    }

    @PostMapping("/customerTableServed")
    @CrossOrigin
    public boolean customerTableServed(@RequestBody CustomerTableModel customerTableModel){
        System.out.println("start customerTableServed");
        waiterRequestService.customerTableServed(customerTableModel.getCustomerTableId());
        return true;
    }

}
