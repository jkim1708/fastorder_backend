package com.fastOrder.fastOrderProject.controllers;

import com.fastOrder.fastOrderProject.entities.MenuItem;
import com.fastOrder.fastOrderProject.repositories.RestaurantRepository;
import com.fastOrder.fastOrderProject.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
public class Controller {

    @Autowired
    MenuItemService menuItemService;


    @Autowired
    RestaurantRepository restaurantRepository;

    @PostMapping("/showMenu")
    @CrossOrigin
    public List<MenuItem> showMenu(@RequestBody Map<String, Object> req) {
        System.out.println(req.get("restaurantId"));
        Long restaurantId = Long.parseLong((String) req.get("restaurantId"));
        return menuItemService.getAllMenuItems(restaurantId);
    }


    //testing connection of restcontroller
    @GetMapping("/ping")
    @CrossOrigin
    public boolean ping(){

        System.out.println("ping angekommen!");
        return true;
    }

}
