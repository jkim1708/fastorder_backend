package com.fastOrder.fastOrderProject.entities;


import org.hibernate.annotations.Cascade;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long restaurantId;

    private String restaurantName;

    @Nullable
    private String restaurantBusinessName;
    @Nullable
    private String restaurantOwnerName;
    @Nullable
    private String restaurantOwnerNumber;
    @Nullable
    private String restaurantOwnerEmail;
    @Nullable
    private String restaurantLocationStreet;
    @Nullable
    private String restaurantLocationPostnumber;
    @Nullable
    private String restaurantLocationCity;
    @Nullable
    private String restaurantLocationCountry;

    @OneToMany
    @JoinColumn(name="restaurantId")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<MenuItem> menuItems;

    @OneToMany
    @JoinColumn(name="restaurantId")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<WaiterRequest> waiterRequests;




    public Restaurant(){}

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantBusinessName() {
        return restaurantBusinessName;
    }

    public void setRestaurantBusinessName(String restaurantBusinessName) {
        this.restaurantBusinessName = restaurantBusinessName;
    }

    public String getRestaurantOwnerName() {
        return restaurantOwnerName;
    }

    public void setRestaurantOwnerName(String restaurantOwnerName) {
        this.restaurantOwnerName = restaurantOwnerName;
    }

    public String getRestaurantOwnerNumber() {
        return restaurantOwnerNumber;
    }

    public void setRestaurantOwnerNumber(String restaurantOwnerNumber) {
        this.restaurantOwnerNumber = restaurantOwnerNumber;
    }

    public String getRestaurantOwnerEmail() {
        return restaurantOwnerEmail;
    }

    public void setRestaurantOwnerEmail(String restaurantOwnerEmail) {
        this.restaurantOwnerEmail = restaurantOwnerEmail;
    }

    public String getRestaurantLocationStreet() {
        return restaurantLocationStreet;
    }

    public void setRestaurantLocationStreet(String restaurantLocationStreet) {
        this.restaurantLocationStreet = restaurantLocationStreet;
    }

    public String getRestaurantLocationPostnumber() {
        return restaurantLocationPostnumber;
    }

    public void setRestaurantLocationPostnumber(String restaurantLocationPostnumber) {
        this.restaurantLocationPostnumber = restaurantLocationPostnumber;
    }

    public String getRestaurantLocationCity() {
        return restaurantLocationCity;
    }

    public void setRestaurantLocationCity(String restaurantLocationCity) {
        this.restaurantLocationCity = restaurantLocationCity;
    }

    public String getRestaurantLocationCountry() {
        return restaurantLocationCountry;
    }

    public void setRestaurantLocationCountry(String restaurantLocationCountry) {
        this.restaurantLocationCountry = restaurantLocationCountry;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public List<WaiterRequest> getWaiterRequests() {
        return waiterRequests;
    }

    public void setWaiterRequests(List<WaiterRequest> waiterRequests) {
        this.waiterRequests = waiterRequests;
    }
}
