package com.fastOrder.fastOrderProject.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WaiterRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long waiterRequestId;

    long customerTableId;
    private long restaurantId;

    private String requestReceivedTimeStamp;

    private boolean customerServed;

    public WaiterRequest() {
    }

    public WaiterRequest(long waiterRequestId) {
        this.waiterRequestId = waiterRequestId;
    }

    public WaiterRequest(long waiterRequestId, long customerTableId) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
    }

    public WaiterRequest(long waiterRequestId, long customerTableId, long restaurantId) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
    }

    public WaiterRequest(long customerTableId, long restaurantId, String requestReceivedTimeStamp, boolean customerServed) {
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
        this.customerServed = customerServed;
    }

    public WaiterRequest(long waiterRequestId, long customerTableId, long restaurantId, String requestReceivedTimeStamp) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
    }

    public WaiterRequest(long waiterRequestId, long customerTableId, long restaurantId, String requestReceivedTimeStamp, boolean customerServed) {
        this.waiterRequestId = waiterRequestId;
        this.customerTableId = customerTableId;
        this.restaurantId = restaurantId;
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
        this.customerServed = customerServed;
    }

    public long getWaiterRequestId() {
        return waiterRequestId;
    }

    public void setWaiterRequestId(long waiterRequestId) {
        this.waiterRequestId = waiterRequestId;
    }

    public String getRequestReceivedTimeStamp() {
        return requestReceivedTimeStamp;
    }

    public void setRequestReceivedTimeStamp(String requestReceivedTimeStamp) {
        this.requestReceivedTimeStamp = requestReceivedTimeStamp;
    }

    public boolean isCustomerServed() {
        return customerServed;
    }

    public void setCustomerServed(boolean customerServed) {
        this.customerServed = customerServed;
    }

    public long getCustomerTableId() {
        return customerTableId;
    }

    public void setCustomerTableId(long customerTableId) {
        this.customerTableId = customerTableId;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
