package com.fastOrder.fastOrderProject.repositories;

import com.fastOrder.fastOrderProject.entities.MenuItem;
import com.fastOrder.fastOrderProject.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant,Long> {

}
