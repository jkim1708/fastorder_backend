package com.fastOrder.fastOrderProject.repositories;

import com.fastOrder.fastOrderProject.entities.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem,Long> {


    List<MenuItem> findByRestaurantId(long restaurantId);

}
