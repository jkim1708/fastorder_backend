package com.fastOrder.fastOrderProject.repositories;


import com.fastOrder.fastOrderProject.entities.WaiterRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WaiterRequestRepository extends JpaRepository<WaiterRequest, Long> {
    List<WaiterRequest> findAllByCustomerTableId(long CustomerTableId);
    WaiterRequest findAllByWaiterRequestId(long WaiterRequestId);
    List<WaiterRequest> findAllWaiterRequestsByRestaurantId(long restaurantId);

}
